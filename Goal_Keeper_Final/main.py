#!/usr/bin/python

import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2
import camera
import gc
import imageProcessor
import motor
import time
import fileHandler

#*********GLOBAL***********
FieldLowerPixelBoandary = None
FieldUpperPixelBoandary = None
GoalKeeperLowerBoundary = None
GoalKeeperUpperBoundary = None
GoalKeeperCurrentPosition = None
GoalKeeperDestinationPosition = None
#**************************

def setFieldPixelUpperBoundary(ballPixelPosition):
    global FieldUpperPixelBoandary
    
    FieldUpperPixelBoandary = ballPixelPosition
    print("Field Upper Boundary = ", FieldUpperPixelBoandary)
    
    
#Sets @Global FieldLowerPixelBoandary and moves
#Goal Keeper to set boundary (sanity check)
def setFieldPixelLowerBoundary(ballPixelPosition):
    global FieldLowerPixelBoandary, GoalKeeperCurrentPosition, GoalKeeperLowerBoundary
    
    FieldLowerPixelBoandary = ballPixelPosition
    print("Field Lower Boundary = ", FieldLowerPixelBoandary)
    GoalKeeperLowerBoundary = 0
    GoalKeeperCurrentPosition = GoalKeeperLowerBoundary
    print("Goal Keepr Lower Boundary = ", GoalKeeperLowerBoundary)
    

def pixelsBetween_CurrentPos_Ball(stepSize, currentStepPosition, ballPixelPosition):
    currentPixelPos = currentStepPosition / stepSize
    return (ballPixelPosition - currentPixelPos)

def getballPixelLocation(objCamera, objImage_processor):    
    circles = None
    
    while circles is None:        
        frame = objCamera.getImage()
        objImage_processor.detectBall(frame)
        circles = objImage_processor.getDetectedBall()
        objCamera.clearStream()
    
    
    for i in circles[0,:]:        
        return round(i[0])

def followBall(motorObj, ballPixelPosition):
    global GoalKeeperUpperBoundary, GoalKeeperCurrentPosition, FieldLowerPixelBoandary, FieldUpperPixelBoandary

    if(ballPixelPosition >= FieldLowerPixelBoandary and ballPixelPosition <= FieldUpperPixelBoandary):
        camWidth = 320
        stepSize = GoalKeeperUpperBoundary / camWidth
        
        pixelBallPaddleDiff = pixelsBetween_CurrentPos_Ball(stepSize, GoalKeeperCurrentPosition, ballPixelPosition)
        print("Pixel paddle diff : ", pixelBallPaddleDiff)
        stepsBallPaddleDiff = abs(pixelBallPaddleDiff * stepSize)
        motorPeriod = int(stepsBallPaddleDiff / motorObj.getMotorFrequency())
        print("Period : ",motorPeriod)
        
        if(pixelBallPaddleDiff < 0):
            GoalKeeperCurrentPosition = GoalKeeperCurrentPosition - motorObj.motorMove("DOWN", motorPeriod)
        
        if(pixelBallPaddleDiff > 0):
            GoalKeeperCurrentPosition = GoalKeeperCurrentPosition + motorObj.motorMove("UP", motorPeriod)
        
        #fileHand.savePosition(GoalKeeperCurrentPosition)

#**************
#    MAIN
#**************
gc.enable() #enable garbage collector
cam = camera.Camera()
motor = motor.Motor()
#fileHand = fileHandler.FileHandler()

imageProc = imageProcessor.ImageProcessor()
#GoalKeeperCurrentPosition = fileHand.getPosition()

#DETECT LOWER BORDER
if((FieldLowerPixelBoandary is None) or (GoalKeeperLowerBoundary is None)):
    print("Calibrating lower pixel boandary. Move ball to motor boundary.")
    ballPixelPosition = getballPixelLocation(cam, imageProc)
    try:
        print("Press interrupt to set Lower Border")
        maxMovePeriod = 20
        motor.motorMove("DOWN", maxMovePeriod)
        while 1:
            print("Wating...")
            time.sleep(2)
    except KeyboardInterrupt:
        setFieldPixelLowerBoundary(ballPixelPosition)
        #fileHand.savePosition(GoalKeeperCurrentPosition)
        pass

#DETECT UPPER BORDER
if((FieldUpperPixelBoandary is None) or (GoalKeeperUpperBoundary is None)):
    print("Calibrating upper pixel boandary. Move ball to upper boundary.")
    ballPixelPosition = getballPixelLocation(cam, imageProc)
    try:
        print("Press interrupt to set Upper Border")
        maxMovePeriod = 20
        stepsMoved = motor.motorMove("UP", maxMovePeriod)
        while 1:
            print("Wating...")
            time.sleep(1)
    except KeyboardInterrupt:
        setFieldPixelUpperBoundary(ballPixelPosition)
        GoalKeeperUpperBoundary = stepsMoved
        GoalKeeperCurrentPosition = GoalKeeperUpperBoundary
        print("Goal Keepr Upper Boundary = ", GoalKeeperUpperBoundary)
        #fileHand.savePosition(GoalKeeperCurrentPosition)
        pass

#CENTER GOALKEEPER
movePeriod = int((GoalKeeperUpperBoundary/2) / motor.getMotorFrequency())
stepsMoved = motor.motorMove("DOWN", movePeriod) #Assume that Goal Keeper is at upper boundary
GoalKeeperCurrentPosition = GoalKeeperUpperBoundary - stepsMoved
#fileHand.savePosition(GoalKeeperCurrentPosition)

while True:
    ballPixelPosition = getballPixelLocation(cam, imageProc)
    followBall(motor, ballPixelPosition)

import cv2
import numpy as np
class ImageProcessor(object):
    # default HSV values (Orange)
    #hmn = 12 #hue min
    #hmx = 37 #hue max
    #smn = 96 #saturation min
    #smx = 255 #saturation max
    #vmn = 186 #value min
    #vmx = 255 #value max
    
    # Green
    hmn = 41
    hmx = 179
    smn = 0
    smx = 255
    vmn = 160
    vmx = 255
    
    # captured frame HSV
    hue = 0
    sat = 0
    val = 0
    
    #filtered frame
    filteredFrame = None
    
    #detected circles
    circles = []
    
    def __init__(self):
        print "INFO: ImageProcessor object created"

    #converting to HSV
    def convertTo_HSV(self,frame):
        hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
        hue,sat,val = cv2.split(hsv)
        self.hue = hue
        self.sat = sat
        self.val = val

    def getThresholding(self):
        hthresh = cv2.inRange(np.array(self.hue),np.array(self.hmn),np.array(self.hmx))
        sthresh = cv2.inRange(np.array(self.sat),np.array(self.smn),np.array(self.smx))
        vthresh = cv2.inRange(np.array(self.val),np.array(self.vmn),np.array(self.vmx))
        return [hthresh,sthresh,vthresh]
    
    def applyMorphologicalFiltering(self):
        kernel = np.ones((5,5),np.uint8)
        arrThreshold = self.getThresholding()
        tracking = cv2.bitwise_and(arrThreshold[0],cv2.bitwise_and(arrThreshold[1],arrThreshold[2]))
        dilation = cv2.dilate(tracking,kernel,iterations = 1)
        self.filteredFrame = cv2.morphologyEx(dilation, cv2.MORPH_CLOSE, kernel)
        self.filteredFrame = cv2.GaussianBlur(self.filteredFrame,(5,5),0)
    
    def detectCircles(self, minRad = 10, maxRad = 0):
        self.circles = cv2.HoughCircles(self.filteredFrame,cv2.cv.CV_HOUGH_GRADIENT,2,120,param1=120,param2=50,minRadius= minRad,maxRadius= maxRad)
    
    def detectBall(self, frame):
        self.convertTo_HSV(frame)
        self.applyMorphologicalFiltering()
    
    def getDetectedBall(self):
        self.detectCircles()
        return self.circles
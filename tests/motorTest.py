import RPi.GPIO as GPIO
import time

#******VARIABLES*******
STEP_PIN = 8
DIRECTION_PIN = 3
MAX_FREQ = 1200
DUTY_CYCLE = 50
freq = 100
#**********************

GPIO.setmode(GPIO.BOARD)
GPIO.setup(STEP_PIN, GPIO.OUT) #STEP
GPIO.setup(DIRECTION_PIN, GPIO.OUT) #direction

GPIO.output(DIRECTION_PIN, 0) #1 =down, 0 = up

pwm = GPIO.PWM(STEP_PIN, freq)

print("INFO: Startup sequence in 2 seconds")
time.sleep(2)
pwm.start(DUTY_CYCLE)

try:
    print("Press button to stop")
    secondsOn = 0
    while 1:
        while freq < MAX_FREQ:
            freq += 100
            pwm.ChangeFrequency(freq)
            time.sleep(0.1)
        
        time.sleep(1)
        secondsOn += 1
        if secondsOn == 6:
            break
except KeyboardInterrupt:
    pass
pwm.stop()
GPIO.cleanup()
print("DONE")
    



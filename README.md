Welcome to my Goal Keeper project
================
This page serves as an instruction manual to create their own ball following machine (Goal Keeper).
This project's initial pupose was as my final year project while studying BTech Electrical at [University of Johannesburg, South Africa](https://www.uj.ac.za/).
____

## Abstract
The problem is interpreting sensor data in order to make a decision. The method used to solve this problem will be to design and build a robotic foosball goalkeeper. The goalkeeper must employ measurements and control in order to intercept an object.

![End Result](./img/header.jpg)

## Design
The design of the of the overall solution is shown in the below drawing.

![Solution Drawing](./img/design.png)

## Components Used
* Raspberry Pi model B
* Raspberry Pi Camera
* Stepper motor (Hybrid Bipolar)
* Threaded Rod
* Lead screw
* Linear guides
* Linear Bearings
* Electrical components 
  * Pololu Driver (DRV8825)
  * Resistors
  * LED
  * 15V regulator (L7805CV)
  * Capacitors
  * NPN Mosfets
  * Transistors (IN4148)
  * Ribbon Cable + Connectors

## Constructing Stepper Driver Circuit
Below drawing details the circuit layout and components.

![Driver Drawing](./img/circuit_design.jpg)
![Driver Completed](./img/driver.jpg)
![Solution Connection](./img/drive_connection.jpg)

## Connect and activate Raspberry Pi camera
Connected camera ribbon cable to raspberry pi HDMI port. Warning; camera module is static sensitive, so grounded myself first.

![Camera](./img/camera.jpg)

use “raspi-config” to activate camera. Note that large portion of memory is allocated to camera when active.

![Camera Config](./img/camera_conf.png)

## Bootstrap Raspberry Pi with OpenCV
Source: http://www.pyimagesearch.com/2015/02/23/install-opencv-and-python-on-your-raspberry-pi-2-and-b/

~~~~
# Update system.
$ sudo apt-get update
# Install required developer tools.
$ sudo apt-get install build-essential cmake pkg-config
# Install the necessary image I/O packages.
$ sudo apt-get install libjpeg8-dev libtiff4-dev libjasper-dev libpng12-dev
# Install the GTK development library.
$ sudo apt-get install libgtk2.0-dev
# Install the necessary video I/O packages.
$ sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
# Install libraries that are used to optimize various operations within OpenCV.
$ sudo apt-get install libatlas-base-dev gfortran
# Install pip.
$ wget https://bootstrap.pypa.io/get-pip.py | python get-pip.py
# Install the Python 2.7.
$ sudo apt-get install python2.7-dev
# Install NumPy.
$ pip install numpy
# Download OpenCV, unpack and build.
$ wget -O opencv-2.4.10.zip http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.10/opencv-2.4.10.zip/download
$ unzip opencv-2.4.10.zip
$ cd opencv-2.4.10
$ mkdir build
$ cd build
$ cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D BUILD_NEW_PYTHON_SUPPORT=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON  -D BUILD_EXAMPLES=ON ..
# Compile OpenCV.
$make
# Install OpenCV
$ sudo make install
$ sudo ldconfig
# Sanity test Python and OpenCV
$ python
>>> import cv2
>>> cv2.__version__
~~~~

## Test Ball Tracking
Run the scripts localted in ./tests 

![Tests](./img/tracking.png)

## Final Steps
Create the playing field

![Play](./img/play_field.jpg)

Mount the Raspverry Pi assembly behind the goal keeper and connect to the stepper motor.

![Final](./img/final.jpg)

Run the application (Goal_Keeper_Final/main.py)

![Teminal](./img/terminal.png)

from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2

class Camera(object):
    capture = None
    piCamera = None
    rawCapture = None
    
    def __init__(self):
        self.initPiCamera()
        
    def initPiCamera(self):
        self.piCamera = PiCamera()
        self.piCamera.resolution = (320, 240)
        self.piCamera.framerate = 90
        self.rawCapture = PiRGBArray(self.piCamera, size=(320, 240))
        
        #allow camera to warm up
        time.sleep(0.5)
        print("INFO: pi camera object created")
        
    def initWebCam(self):
        self.capture = cv2.VideoCapture(0)
        self.setWebCamResolution()
        print("INFO: web camera object created")
    
    def setWebCamResolution(self, width = 320, height = 240):
        # Reduce the size of video to 320x240 so can process faster
        self.capture.set(3,width)
        self.capture.set(4,height)
    
    def getImage(self):
        #Check if using pi camera or web cam
        if self.piCamera is not None:
            self.piCamera.capture(self.rawCapture, format = "bgr", use_video_port=True)
            return self.rawCapture.array
        return self.capture.read()
    
    # clear the stream in preparation for the next frame
    def clearStream(self):
        self.rawCapture.truncate(0)
    
    def __del__(self):
        if self.capture is not None:
            self.capture.release()
        print "INFO: camera released"
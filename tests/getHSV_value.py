import sys
import numpy as np
sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2
#BGR
red  = np.uint8([[[0,0,255 ]]])
hsv_red = cv2.cvtColor(red,cv2.COLOR_BGR2HSV)
print hsv_red

green = np.uint8([[[0,255,0 ]]])
hsv_green = cv2.cvtColor(green,cv2.COLOR_BGR2HSV)
print hsv_green

blue = np.uint8([[[255,0,0 ]]])
hsv_blue = cv2.cvtColor(blue,cv2.COLOR_BGR2HSV)
print hsv_blue

#[H-10, 100,100] and [H+10, 255, 255] as lower bound and upper bound respectively
import RPi.GPIO as GPIO
import time

class Motor(object):
    #******CONSTANTS*******
    STEP_PIN = 8
    DIRECTION_PIN = 3
    MAX_FREQ = 1200
    STEP_UP = 1
    STEP_DOWN = 0
    
    #******VARIABLES*******
    DUTY_CYCLE = 50
    freq = 500
    objPWM = None
    
    def __init__(self):
        self.initPorts()
        print("INFO: GPIO initialized")
        
    
    def initPorts(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.STEP_PIN, GPIO.OUT) #STEP
        GPIO.setup(self.DIRECTION_PIN, GPIO.OUT) #direction
        self.objPWM = GPIO.PWM(self.STEP_PIN, self.freq)
    
    #Initial slow speed strartup
    def initMotorStartup(self):
        self.objPWM.start(self.DUTY_CYCLE)
        while self.freq < self.MAX_FREQ:
            self.freq += 100
            self.objPWM.ChangeFrequency(self.freq)
            time.sleep(0.1)
            print("Motor freq : ",self.freq)
    
    
    def motorDirectionUP(self):
        GPIO.output(self.DIRECTION_PIN, GPIO.LOW) #1 = down, 0 = up
    
    def motorDirectionDown(self):
        GPIO.output(self.DIRECTION_PIN, GPIO.HIGH) #1 = down, 0 = up
    
    def motorMove(self, direction, period):
        movedPeriod = 0;
        if direction == "UP" or direction == 0:
            self.motorDirectionUP()
        else:
            self.motorDirectionDown()
        
        self.DUTY_CYCLE = 50
        self.initMotorStartup()
        try:
            for t in range(0, period):
                time.sleep(1)
                movedPeriod += 1
        except KeyboardInterrupt:
            pass
        
        self.objPWM.stop()
        steps = movedPeriod * self.MAX_FREQ
        return steps
    
    def getMotorFrequency(self):
        return self.MAX_FREQ
    
    def __del__(self):
        self.objPWM.stop()
        GPIO.cleanup()
        print("INFO: Motor stopped")

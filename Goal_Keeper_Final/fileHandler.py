#!/usr/bin/python

class FileHandler(object):
    file = None
    
    def __init__(self):
        try:
            # Open a file
            self.file = open("goalkeeperPosition.txt", "r")
            print("INFO: File Opened")
            self.file.close()
        except IOError as e:
            #create file
            print("INFO: New file with default half field (7200) created")
            self.file = open("goalkeeperPosition.txt", "w")
            self.file = open("goalkeeperPosition.txt", "w")
            self.file.write("7200")
            self.file.close()
            pass
    
    def savePosition(self, currentPosition):
        self.file = open("goalkeeperPosition.txt", "w")
        self.file.write(str(currentPosition))
        self.file.close()
    
    def getPosition(self):
        self.file = open("goalkeeperPosition.txt", "r")
        pos = self.file.read()
        self.file.close()
        return int(pos)
    
    def __del__(self):
        # Close opend file
        self.file.close()
        print("INFO: File closed")
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2
import numpy as np

img = cv2.imread('../out.jpg');

px = img[100,100]
print px

#blue pixel only
e1 = cv2.getTickCount();
blue = img[100,100,0]
e2 = cv2.getTickCount();
print blue
time  = (e2 - e1)/ cv2.getTickFrequency()
print "Time Taken = %f" % (time)

#better pixel access
e1 = cv2.getTickCount();
print img.item(100,100,0)#returns blue val (100,100,1) return green pixel etc.
e2 = cv2.getTickCount();
time  = (e2 - e1)/ cv2.getTickFrequency()
print "Time Taken = %f" % (time)

print img.shape #returns a tuple of number of rows, columns and channels (if image is color)

print img.dtype #image data type

print cv2.useOptimized() #check if OpenCV is optimized